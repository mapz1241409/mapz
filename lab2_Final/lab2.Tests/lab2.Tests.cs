using lab2;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace lab2.Tests
{
    public class lab2
    {
        List<string> links1 = new List<string>()
          {
               "netflix.com",
               "kinopoisk.ru"
          };
        List<string> links2 = new List<string>()
          {
               "rezka.ag",
               "kinopoisk.ru"
          };
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void FindMovieGenreIDs()
        {
            // arrange

            List<uint> expected = new List<uint>() { 1, 2, 3, 4, 5 };

            // act

            ModelTests modelTests = new ModelTests();

            var result = modelTests.Movies.Select(p => p.GenreID).Distinct().ToList().OrderBy(p => p);

            // assert

            Assert.IsTrue(result.SequenceEqual(expected));
        }

        [Test]
        public void FindMovieWithGenreID_3()
        {
            // arrange

            List<Movie> expected = new List<Movie>()
               {
                    new Movie("Big Lebowski", 3, 1.98, true, links2),
                    new Movie("City Lights", 3, 1.35, false, links2),
                    new Movie("Dr Strangelove: or, how I learned to stop worrying and love the bomb", 3, 1.58, false, links2)
               };
            expected.Sort(new MovieComparerByNameLength());

            // act

            ModelTests modelTests = new ModelTests();

            List<Movie> Movies = new List<Movie>();
            var res = 

            var result = modelTests.Movies.Where(p => p.GenreID == 3).ToList().OrderBy(p => p, new MovieComparerByNameLength());

            // assert
            Assert.IsTrue(result.SequenceEqual(expected));
        }

        [Test]
        public void FindMovieGenres()
        {
            // arrange

            var expected = new List<Genre>()
               {
                    new Genre("Criminal", 1, 8.8),
                    new Genre("Philosophical", 2, 3.2),
                    new Genre("Comedy", 3, 9.3),
                    new Genre("Drama", 4, 7.8),
                    new Genre("Science fiction", 5, 7.5)
               };

            // act

            ModelTests modelTests = new ModelTests();
            List<Genre> genres = new List<Genre>();
            var genresIDs = modelTests.Movies.Select(p => p.GenreID).Distinct().ToList();
            foreach (var genreID in genresIDs)
            {
                genres.Add(modelTests.GenresDict[(int)genreID]);
            }
            genres.Sort(new GenreComparer());

            // assert
            Assert.IsTrue(genres.SequenceEqual(expected));
        }

        [Test]
        public void GetMovieGenre()
        {
            // arrange

            Genre expected = new Genre("Criminal", 1, 8.8);

            // act
            ModelTests modelTests = new ModelTests();
            var genre = modelTests.Movies[0].GetGenre(modelTests.GenresDict);

            // assert
            Assert.IsTrue(genre.Equals(expected));
        }

        [Test]
        public void SortMoviesByDuration()
        {
            // arrange

            var expected = new List<Movie>()
               {
                    new Movie("City Lights", 3, 1.35, false, links2),
                    new Movie("Dr Strangelove: or, how I learned to stop worrying and love the bomb", 3, 1.58, false, links2),
                    new Movie("Seventh seal", 2, 1.6, false, links1),
                    new Movie("Autumn sonata", 4, 1.65, true, links1),
                    new Movie("Requiem for a dream", 4, 1.7, false, links2),
                    new Movie("School ties", 4, 1.78, true, links2),
                    new Movie("Taxi driver", 1, 1.9, false, links1),
                    new Movie("Big Lebowski", 3, 1.98, true, links2),
                    new Movie("Good Will Hunting", 4, 2.1, true, links1),
                    new Movie("Melancholia", 5, 2.26, false, links2),
                    new Movie("2001: a space odyssey", 5, 2.31, true, links2),
                    new Movie("Goodfellas", 1, 2.43, false, links1),
                    new Movie("Sacrifice", 2, 2.48, true, links1),
                    new Movie("There will be blood", 2, 2.6, true, links2),
                    new Movie("Solaris", 5, 2.7, true, links2),
                    new Movie("Stalker", 5, 2.71, true, links2),
                    new Movie("Pulp Fiction", 1, 2.75, true, links1),
               };

            // act

            ModelTests modelTests = new ModelTests();
            var result = modelTests.Movies.OrderBy(p => p, new MovieComparerByDuration());

            //assert
            Assert.IsTrue(result.SequenceEqual(expected));
        }

        [Test]
        public void MovieQueue()
        {
            // arrange
            Queue<Movie> expected = new Queue<Movie>();
            expected.Append(new Movie("City Lights", 3, 1.35, false, links2));
            expected.Append(new Movie("Dr Strangelove: or, how I learned to stop worrying and love the bomb", 3, 1.58, false, links2));
            expected.Append(new Movie("Seventh seal", 2, 1.6, false, links1));
            expected.Append(new Movie("Autumn sonata", 4, 1.65, true, links1));

            Queue<Movie> result = new Queue<Movie>();

            // act

            ModelTests modelTests = new ModelTests();
            foreach (var item in modelTests.Movies.Where(p => p.Duration < 100).OrderBy(p => p, new MovieComparerByDuration()))
            {
                result.Append(item);
            }

            // assert
            Assert.IsTrue(result.SequenceEqual(expected));
        }

        [Test]
        public void SortMoviesByName()
        {
            // arrange

            List<Movie> expected = new List<Movie>()
               {
                    new Movie("2001: a space odyssey", 5, 2.31, true, links2),
                    new Movie("Autumn sonata", 4, 1.65, true, links1),
                    new Movie("Big Lebowski", 3, 1.98, true, links2),
                    new Movie("City Lights", 3, 1.35, false, links2),
                    new Movie("Dr Strangelove: or, how I learned to stop worrying and love the bomb", 3, 1.58, false, links2),
                    new Movie("Good Will Hunting", 4, 2.1, true, links1),
                    new Movie("Goodfellas", 1, 2.43, false, links1),
                    new Movie("Melancholia", 5, 2.26, false, links2),
                    new Movie("Pulp Fiction", 1, 2.75, true, links1),
                    new Movie("Requiem for a dream", 4, 1.7, false, links2),
                    new Movie("Sacrifice", 2, 2.48, true, links1),
                    new Movie("School ties", 4, 1.78, true, links2),
                    new Movie("Seventh seal", 2, 1.6, false, links1),
                    new Movie("Solaris", 5, 2.7, true, links2),
                    new Movie("Stalker", 5, 2.71, true, links2),
                    new Movie("Taxi driver", 1, 1.9, false, links1),
                    new Movie("There will be blood", 2, 2.6, true, links2),
               };

            // act
            ModelTests model = new ModelTests();
            var result = model.Movies.OrderBy(p => p.Name).ToList();

            // assert
            Assert.IsTrue(result.SequenceEqual(expected));
        }

        [Test]
        public void InitAndAnonymous()
        {
            // arrange

            // initializer
            Movie movieInit = new Movie() { Name = "Clockwork orange", GenreID = 1, Duration = 2.26, ShownTheseDays = true, LinksWhereWatch = links1 };
            Movie movieResult = new Movie("Clockwork orange", 1, 2.26, true, links1);


            // anonymous class
            var customMovie = new { Name = "Barry Lyndon", Director = "Stanley Kubrick", Genre = "Historical movie", Duration = 3.04 };

            // act
            bool init = movieInit.Equals(movieResult);
            bool custom = customMovie.Name == "Barry Lyndon" && customMovie.Director == "Stanley Kubrick"
                 && customMovie.Genre == "Historical movie" && customMovie.Duration == 3.04;

            // assert
            Assert.IsTrue(init && custom);
        }
    }
}