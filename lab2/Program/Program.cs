﻿using lab2;

class Program
{
    public static void Main(string[] args)
    {
        List<string> links1 = new List<string>()
          {
               "netflix.com",
               "kinopoisk.ru"
          };
        List<string> links2 = new List<string>()
          {
               "rezka.ag",
               "kinopoisk.ru"
          };

        // initializer
        Movie movie1 = new Movie() { Name = "Clockwork orange", GenreID = 1, Duration = 2.26 };
        Console.WriteLine("Initializer 1:");
        Console.WriteLine(
             string.Format(
             "Name: {0} - GenreID: {1} - Duration: {2}h",
             movie1.Name,
             movie1.GenreID,
             movie1.Duration
             ));
        Console.WriteLine();

        Console.WriteLine("Initializer 2:");
        Movie movie2 = new Movie() { Name = "Black swan", GenreID = 4, Duration = 1.8 };
        Console.WriteLine(
             string.Format(
             "Name: {0} - GenreID: {1} - Duration: {2}h",
             movie2.Name,
             movie2.GenreID,
             movie2.Duration
             ));
        Console.WriteLine();

        Console.WriteLine("Initializer 3:");
        Movie movie3 = new Movie() { Name = "Sacrifice", GenreID = 2, Duration = 2.48 };
        Console.WriteLine(
             string.Format(
             "Name: {0} - GenreID: {1} - Duration: {2}h",
             movie3.Name,
             movie3.GenreID,
             movie3.Duration
             ));
        Console.WriteLine();

        // anonymous class
        var customMovie1 = new { Name = "Barry Lyndon", Director = "Stanley Kubrick", Genre = "Historical movie", Duration = 3.04 };
        Console.WriteLine("Anonymous class 1:");
        Console.WriteLine(
             string.Format(
             "Name: {0} - Director: {1} - Genre: {2} - Duration: {3}h",
             customMovie1.Name,
             customMovie1.Director,
             customMovie1.Genre,
             customMovie1.Duration
             ));
        Console.WriteLine();

        Console.WriteLine("Anonymous class 2:");
        var customMovie2 = new { Name = "The shining", Director = "Stanley Kubrick", Rating = 8.8, Duration = 2.36 };
        Console.WriteLine(
             string.Format(
             "Name: {0} - Director: {1} - Rating: {2} - Duration: {3}h",
             customMovie2.Name,
             customMovie2.Director,
             customMovie2.Rating,
             customMovie2.Duration
             ));
        Console.WriteLine();

        Console.WriteLine("Anonymous class 3:");
        var customMovie3 = new { Name = "Eyes wide shut", Country = "USA", Duration = 2.65 };
        Console.WriteLine(
             string.Format(
             "Name: {0} - Country: {1} - Duration: {2}h",
             customMovie3.Name,
             customMovie3.Country,
             customMovie3.Duration
             ));
        Console.WriteLine();

        List<Movie> movieList1 = new List<Movie>()
          {
               new Movie("There will be blood", 2, 2.63, true, links2),
               new Movie("Big Lebowski", 3, 1.98, false, links2),
               new Movie("Seventh seal", 2, 1.6, false, links2),
               new Movie("Requiem for a dream", 4, 1.7, true, links1),
          };

        List<Movie> movieList2 = new List<Movie>()
          {
               new Movie("Taxi driver", 1, 1.9, false, links1),
               new Movie("Big Lebowski", 3, 1.98, true, links2),
               new Movie("Good Will Hunting", 4, 2.1, true, links1),
          };

        List<Movie> movieList3 = new List<Movie>()
          {
               new Movie("There will be blood", 2, 2.6, true, links2),
               new Movie("Solaris", 5, 2.7, true, links2),
               new Movie("Stalker", 5, 2.71, true, links2),
          };


        Console.WriteLine("List 1:");
        foreach (var item in movieList1)
        {
            PrintMovie(item);
        }
        Console.WriteLine();

        Console.WriteLine("Array 1: ");
        Movie[] movieArr1 = movieList1.ToArray();
        foreach (var item in movieArr1)
        {
            PrintMovie(item);
        }
        Console.WriteLine();

        Console.WriteLine("List 2:");
        foreach (var item in movieList2)
        {
            PrintMovie(item);
        }
        Console.WriteLine();

        Console.WriteLine("Array 2: ");
        Movie[] movieArr2 = movieList2.ToArray();
        foreach (var item in movieArr2)
        {
            PrintMovie(item);
        }
        Console.WriteLine();

        Console.WriteLine("List 3:");
        foreach (var item in movieList1)
        {
            PrintMovie(item);
        }
        Console.WriteLine();

        Console.WriteLine("Array 3: ");
        Movie[] movieArr3 = movieList3.ToArray();
        foreach (var item in movieArr3)
        {
            PrintMovie(item);
        }
        Console.WriteLine();

        var genreIDs1 = movieList1.GroupBy(p => p.GenreID).ToList();
        var genreIDs2 = movieList2.GroupBy(p => p.GenreID).ToList();
        var genreIDs3 = movieList3.GroupBy(p => p.GenreID).ToList();

        Console.WriteLine("Grouping 1:");
        foreach (var id in genreIDs1)
        {
            Console.WriteLine("ID - {0}", id.Key);
            foreach (var mv in id)
            {
                Console.WriteLine("{0}", mv.Name);
            }
            Console.WriteLine();
        }

        Console.WriteLine("Grouping 2:");
        foreach (var id in genreIDs2)
        {
            Console.WriteLine("ID - {0}", id.Key);
            foreach (var mv in id)
            {
                Console.WriteLine("{0}", mv.Name);
            }
            Console.WriteLine();
        }

        Console.WriteLine("Grouping 3:");
        foreach (var id in genreIDs3)
        {
            Console.WriteLine("ID - {0}", id.Key);
            foreach (var mv in id)
            {
                Console.WriteLine("{0}", mv.Name);
            }
            Console.WriteLine();
        }


        /*ModelTests model = new ModelTests();
        var result = model.Movies.OrderBy(p => p.Name).ToList();

        foreach (var item in result)
        {
             Console.WriteLine(item.Name);
        }*/
    }

    public static void PrintMovie(Movie movie)
    {
        Console.Write(
             string.Format(
             "Name: {0} - GenreID: {1} - Duration: {2}h - Shown now: {3} - Watch online: ",
             movie.Name,
             movie.GenreID,
             movie.Duration,
             movie.ShownTheseDays
             ));
        foreach (var item in movie.LinksWhereWatch)
        {
            Console.Write(item);
            Console.Write(' ');
        }
        Console.WriteLine();
    }

}