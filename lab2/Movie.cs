﻿using System.Runtime.CompilerServices;

namespace lab2
{
    public class MovieComparerByGenre : IComparer<Movie>
    {
        public int Compare(Movie x, Movie y)
        {
            if (x == null || y == null)
                throw new ArgumentNullException("Uncorrect parameters");
            return (int)x.GenreID - (int)y.GenreID;
        }
    }

    public class MovieComparerByDuration : IComparer<Movie>
    {
        public int Compare(Movie x, Movie y)
        {
            if (x == null || y == null)
                throw new ArgumentNullException("Uncorrect parameters");
            if (x.Duration > y.Duration)
                return 1;
            if (x.Duration < y.Duration)
                return -1;
            return 0;
        }
    }

    public class MovieComparerByNameLength : IComparer<Movie>
    {
        public int Compare(Movie x, Movie y)
        {
            if (x == null || y == null)
                throw new ArgumentNullException("Uncorrect parameters");
            return x.Name.Length - y.Name.Length;
        }
    }

    public class GenreComparer : IComparer<Genre>
    {
        public int Compare(Genre x, Genre y)
        {
            if (x == null || y == null)
                throw new ArgumentNullException("Uncorrect parameters");
            return x.ID - y.ID;
        }
    }

    public class Movie
    {
        public string Name { get; set; }
        public uint GenreID { get; set; }
        public double Duration { get; set; }
        public bool ShownTheseDays { get; set; }
        public List<string> LinksWhereWatch { get; set; }
        public Movie(string name = "", uint genreID = 0, double duration = 0, bool shownTheseDays = false, List<string> links = null)
        {
            Name = name;
            GenreID = genreID;
            Duration = duration;
            ShownTheseDays = shownTheseDays;
            if (links != null)
                LinksWhereWatch = new List<string>(links);
            else
                LinksWhereWatch = new List<string>();
        }

        public override bool Equals(object? obj)
        {
            if (!(obj is Movie))
                return false;
            Movie movie = (Movie)obj;
            return Name.Equals(movie.Name) && GenreID.Equals(movie.GenreID) && Duration.Equals(movie.Duration) && ShownTheseDays.Equals(movie.ShownTheseDays) && LinksWhereWatch.SequenceEqual(movie.LinksWhereWatch);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    public class Genre
    {
        // IDs
        // 1 - criminal
        // 2 - philosophical
        // 3 - comedy
        // 4 - drama
        // 5 - science fiction

        public string Name { get; set; }
        public int ID { get; set; }
        public double Popularity { get; set; }

        public Genre(string name, int id, double popularity)
        {
            Name = name;
            ID = id;
            Popularity = popularity;
        }

        public override bool Equals(object? obj)
        {
            if (!(obj is Genre))
                return false;
            Genre genre = (Genre)obj;
            return Name.Equals(genre.Name) && ID.Equals(genre.ID) && Popularity.Equals(genre.Popularity);
        }
    }

    public class ModelTests
    {
        public List<Movie> Movies { get; set; }
        public List<Genre> Genres { get; set; }
        public Dictionary<int, Genre> GenresDict { get; set; }
        public List<int> Ints { get; set; }
        public List<double> Doubles { get; set; }
        public List<string> Strings { get; set; }
        public List<char> Chars { get; set; }

        public ModelTests()
        {
            List<string> links1 = new List<string>()
               {
                    "netflix.com",
                    "kinopoisk.ru"
               };
            List<string> links2 = new List<string>()
               {
                    "rezka.ag",
                    "kinopoisk.ru"
               };
            Movies = new List<Movie>()
               {
                    new Movie("Pulp Fiction", 1, 2.75, true, links1),
                    new Movie("Sacrifice", 2, 2.48, true, links1),
                    new Movie("City Lights", 3, 1.35, false, links2),
                    new Movie("Taxi driver", 1, 1.9, false, links1),
                    new Movie("There will be blood", 2, 2.6, true, links2),
                    new Movie("Big Lebowski", 3, 1.98, true, links2),
                    new Movie("Seventh seal", 2, 1.6, false, links1),
                    new Movie("Requiem for a dream", 4, 1.7, false, links2),
                    new Movie("Dr Strangelove: or, how I learned to stop worrying and love the bomb", 3, 1.58, false, links2),
                    new Movie("2001: a space odyssey", 5, 2.31, true, links2),
                    new Movie("Good Will Hunting", 4, 2.1, true, links1),
                    new Movie("Autumn sonata", 4, 1.65, true, links1),
                    new Movie("Solaris", 5, 2.7, true, links2),
                    new Movie("Goodfellas", 1, 2.43, false, links1),
                    new Movie("Melancholia", 5, 2.26, false, links2),
                    new Movie("School ties", 4, 1.78, true, links2),
                    new Movie("Stalker", 5, 2.71, true, links2),
               };

            Genres = new List<Genre>()
               {
                    new Genre("Criminal", 1, 8.8),
                    new Genre("Philosophical", 2, 3.2),
                    new Genre("Comedy", 3, 9.3),
                    new Genre("Drama", 4, 7.8),
                    new Genre("Science fiction", 5, 7.5)
               };

            GenresDict = new Dictionary<int, Genre>()
            {
                [1] = new Genre("Criminal", 1, 8.8),
                [2] = new Genre("Philosophical", 2, 3.2),
                [3] = new Genre("Comedy", 3, 9.3),
                [4] = new Genre("Drama", 4, 7.8),
                [5] = new Genre("Science fiction", 5, 7.5)
            };
        }
    }

    public static class MovieExtensions
    {
        public static Genre GetGenre(this Movie movie, Dictionary<int, Genre> genres)
        {
            return genres[(int)movie.GenreID];
        }
    }
}
