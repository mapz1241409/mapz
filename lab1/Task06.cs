﻿namespace lab1
{
    interface IEnemy
    {
        void Attack();
        void TakeDamage(double damage);
    }

    interface IMovable
    {
        void Move(double distance);
    }

    class Skeleton : IEnemy, IMovable
    {
        public void Attack()
        {
            Console.WriteLine("Attacked");
        }

        public void Move(double distance)
        {
            Console.WriteLine($"Moved {distance} meters");
        }

        public void TakeDamage(double damage)
        {
            Console.WriteLine($"Took {damage} damage");
        }
    }
}
