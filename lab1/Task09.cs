﻿namespace lab1
{
    class RefOutTest
    {
        public string GetTextRef(ref int refid)
        {
            string returnText = "Ref-" + refid.ToString();
            refid += 1;
            return returnText;
        }

        public string GetTextOut(out int outid)
        {
            outid = 1;
            string returnText = "Out-" + outid.ToString();
            return returnText;
        }
    }
}
