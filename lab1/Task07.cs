﻿namespace lab1
{
    class Creature
    {
        protected string name;
        public Creature()
        {
            this.name = "Empty";
        }
        public Creature(string name)
        {
            this.name = name;
        }
        public void DisplayInfo()
        {
            Console.WriteLine($"Creature name: {name}");
        }
    }

    class Rat : Creature
    {
        string subspecies;
        public Rat() : base()
        {
            this.subspecies = "Empty";
        }

        public Rat(string name, string subspecies) : base(name)
        {
            this.subspecies = subspecies;
        }

        public void Squeak()
        {
            Console.WriteLine($"{name} squaked!");
        }

        public new void DisplayInfo()
        {
            Console.WriteLine($"Name: {name}, subspecies: {subspecies}");
        }

    }

}
