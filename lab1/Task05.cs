﻿
namespace lab1
{
    enum Week
    {
        Monday = 1,  
        Tuesday = Monday * 2,   
        Wednesday = Tuesday * 2,   
        Thursday = Wednesday * 2,  
        Friday = Thursday * 2,
        Saturday = Friday * 2,   
        Sunday = Saturday * 2 
    }

    class Test
    {
        Week allDays = Week.Monday | Week.Tuesday | Week.Wednesday | Week.Thursday | Week.Friday
            | Week.Saturday | Week.Sunday;

        Week weekends = Week.Saturday | Week.Sunday;
        Week workingDays = Week.Wednesday | Week.Tuesday | Week.Thursday | Week.Friday;

        private void Operations()
        {
            Week workAllWeek = weekends & workingDays;
            Week noWorkDays = (allDays | weekends) ^ workingDays;

            bool isFreeTheseDays = (workingDays) == 0 && (workingDays & Week.Monday) == 0;
            bool isThereFreeDay = (workingDays) == 0 || (workingDays & Week.Monday) == 0;
            bool isThereWorkDays = !((workingDays) == 0 && (workingDays & Week.Monday) == 0);
        }

    }
}
