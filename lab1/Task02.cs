﻿namespace lab1
{
    class accessModifiersParent
    {
        private int privateField;
        public int publicField;
        internal int internalField;
        protected int protectedField;

        void accessFields()
        {
            privateField = 1;
            protectedField = 2;
            publicField = 3;
            internalField = 4;
        }
    }

    class accessModifiersTest
    {
        accessModifiersParent testClass = new accessModifiersParent();

        void func()
        {
            testClass.publicField = 1;
            testClass.internalField = 2;
            testClass.protectedField = 3;
            testClass.privateField = 4;
        }
    }

    class accessModifiersChildTest : accessModifiersParent
    {
        void func()
        {
            publicField = 1;
            internalField = 2;
            protectedField = 3;
            privateField = 4;
        }
    }
}
