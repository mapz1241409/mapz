﻿namespace lab1
{
    class TestClass
    {
        public double data;

        TestClass()
        {
            data = 0;
            Console.WriteLine("Initialized first");
        }

        public TestClass(double data)
        {
            this.data = data;
            Console.WriteLine("Initialized second");
        }

        public void Test()
        {
            TestClass firstClass = new TestClass(5) { data = 5 };
            Console.WriteLine(firstClass.data); //5;
        }

        public void BoxingTest()
        {
            TestClass secondClass = new TestClass(10);

            object obj = (object)secondClass;
            TestClass secondClassUnboxing = (TestClass)obj; 
        }
    }
}
