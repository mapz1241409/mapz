﻿namespace lab1
{
    class Main
    {
        private class InnerPrivate
        {

        }

        protected class InnerProtected
        {

        }

        InnerPrivate innerPrivate;
        InnerProtected innerProtected;
    }

    class Task04 : Main
    {
        InnerProtected innerProtected;
        InnerPrivate innerPrivate;
    }
}
