﻿namespace lab1
{
    using System.Diagnostics;
    class Program
    {
        public static void Main(string[] args)
        {
            int objAmount = 1000000;

            Console.WriteLine($"C#. Object amount: {objAmount}");

            Stopwatch firstTimer = new Stopwatch();
            firstTimer.Start();
            for (int i = 0; i < objAmount; i++)
            {
                using (TestStructure myStruct = new TestStructure())
                {
                    myStruct.ChangeValue();
                }
            }
            firstTimer.Stop();
            Console.WriteLine($"Struct creation and operations elapsed time: {firstTimer.ElapsedMilliseconds}ms");


            Stopwatch secondTimer = new Stopwatch();
            secondTimer.Start();
            for (int i = 0; i < objAmount; i++)
            {
                using (TestStructure myStruct = new TestStructure())
                {
                }
            }
            secondTimer.Stop();
            Console.WriteLine($"Struct destruction elapsed time: {secondTimer.ElapsedMilliseconds}ms");

        }

        struct TestStructure : IDisposable
        {
            private int value;
            public void ChangeValue() => value = 1;
            public void Dispose()
            {
                GC.SuppressFinalize(this);
            }
        }
    }

}
