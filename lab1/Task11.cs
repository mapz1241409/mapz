﻿namespace lab1
{
    class Number
    {
        public int value;
        public Number(int value)
        {
            this.value = value;
        }
        public static implicit operator int(Number number) => number.value;
        public static explicit operator Number(int number) => new Number(number);

        public void TestFunction()
        {
            Number num1 = new Number(1);

            int num2 = num1; //implicit
            num1 = (Number)5; //explicit
        }
    }

  

}
