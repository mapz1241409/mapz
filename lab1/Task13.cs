﻿class Figure
{
    public double width;
    public double length;
    public double height;

    public Figure(double width = 0, double length = 0, double height = 0)
    {
        this.width = width;
        this.length = length;
        this.height = height;
    }

    public override bool Equals(object? obj)
    {
        if (!(obj is Figure))
            return false;
        Figure figure = (Figure)obj;
        if (figure.width == this.width && figure.length == this.length && figure.height == this.height)
            return true;
        else
            return false;
    }

    public static new bool Equals(object? a, object? b)
    {
        return a.Equals(b);
    }

    public new Type GetType()
    {
        return base.GetType();
    }

    public static new bool ReferenceEquals(Object? a, Object? b)
    {
        return (a == b);
    }

    protected new object MemberwiseClone(object? obj)
    {
        return base.MemberwiseClone();
    }

    public override string ToString()
    {
        return $"Width: {width}, Length: {length}, Height: {height}";
    }
}
