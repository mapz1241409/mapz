#include <iostream>
#include <chrono>

using namespace std;

struct TestStructure
{
public:
	int value;
	void changeValue()
	{
		value = 1;
	};
};

int main()
{
	const int objAmount = 1000000;
	cout << "C++. Object amount: " << objAmount;

	auto firstTimerStart = chrono::steady_clock::now();
	for (int i = 0; i < objAmount; i++)
	{
		TestStructure myStruct;
		myStruct.changeValue();
	}
	auto firstTimerEnd = chrono::steady_clock::now();

	auto firstTime = firstTimerEnd - firstTimerStart;

	cout << "Object creation and operations elapsed time: " << chrono::duration<double, milli>(firstTime).count() << "ms" << endl;

	auto secondTimerStart = chrono::steady_clock::now();
	for (int i = 0; i < objAmount; i++)
	{
		TestStructure myStruct;
	}
	auto secondTimerEnd = chrono::steady_clock::now();

	auto secondTime = secondTimerEnd - secondTimerStart;

	cout << "Object destruction elapsed time: " << chrono::duration<double, milli>(secondTime).count() << "ms" << endl;
}

