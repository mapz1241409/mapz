﻿namespace lab1
{
    interface IShootable
    {
        void Shoot(int secondsInCombat);
    }

    abstract class ArmoredVehicle : IShootable
    {
        protected double reloadSpeed;
        protected double ammunitionAmount;
        public abstract void Shoot(int secondsInCombat);
    }

    class Tank : ArmoredVehicle
    {
        public Tank(double reloadSpeed, double ammunitionAmount)
        {
            this.reloadSpeed = reloadSpeed;
            this.ammunitionAmount = ammunitionAmount;
        }

        public override void Shoot(int secondsInCombat)
        {
            if (secondsInCombat < (reloadSpeed * ammunitionAmount))
            {
                Console.WriteLine("Tank's ammunition is sufficient for combat");
            }
            else
            {
                Console.WriteLine("Tank's ammunition is insufficient for combat");
            }
        }
    }
}


