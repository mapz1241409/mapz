﻿public class Lab4
{
    public static void Main()
    {
        Controller controller = new Controller();
        controller.Update();
    }
}
public interface Objective
{
    public List<PropertyContainer> getResists();
}
public class ObjectiveImpl : Objective
{
    protected List<PropertyContainer> resists;
    public List<PropertyContainer> getResists() { return resists; }
    public ObjectiveImpl(PropertyContainer BuyingWorkResist, PropertyContainer LearningResist, int time)
    {
        resists = new List<PropertyContainer>();
        resists.Add(BuyingWorkResist);
        resists.Add(LearningResist);
        this.time = time;
    }
    public int time { get; set; }

    public void SetMemento(ObjectiveImplMemento memento)
    {
        for (int i = 0; i < memento.resists.Count; i++)
        {
            resists[i].resistNumber = memento.resists[i];
        }

        time = memento.time;
    }

    public ObjectiveImplMemento CreateMemento()
    {
        List<int> stateList = new List<int>();

        for (int i = 0; i < resists.Count; i++)
        {
            stateList.Add(resists[i].resistNumber);
        }
        return new ObjectiveImplMemento(stateList, time);
    }
}

public class ObjectiveImplMemento
{
    public ObjectiveImplMemento(List<int> state1, int state2)
    {
        resists = state1;
        time = state2;
    }
    public List<int> resists { get; set; }
    public int time { get; set; }
}

public class AddResistDecorator : Objective
{
    private Objective objective;
    private int resistNumber;
    public AddResistDecorator(Objective objective, int resistNumber)
    {
        this.objective = objective;
        this.resistNumber = resistNumber;
    }
    public List<PropertyContainer> getResists()
    {
        objective.getResists().Add(new PropertyContainer(resistNumber));
        return objective.getResists();
    }
}
public class DeleteResistDecorator : Objective
{
    private int index;
    private Objective objective;
    public DeleteResistDecorator(Objective objective, int index)
    {
        this.objective = objective;
        this.index = index;
    }
    public List<PropertyContainer> getResists()
    {
        objective.getResists().RemoveAt(index);
        return objective.getResists();
    }
}










public class PropertyContainer
{
    public PropertyContainer(int resistNumber)
    {
        this.resistNumber = resistNumber;
    }
    public int resistNumber { get; set; }
}

public class Lab : ObjectiveImpl
{
    public Lab(int buyingWorkResistTmp, int learningResistTmp, int collectiveEffortResistTmp, int timeTmp)
        : base(new PropertyContainer(buyingWorkResistTmp), new PropertyContainer(learningResistTmp), timeTmp)
    {
        resists.Add(new PropertyContainer(collectiveEffortResistTmp));
    }

}

public class Coursework : ObjectiveImpl
{
    public Coursework(int buyingWorkResistTmp, int learningResistTmp, int writeOffResist, int timeTmp)
        : base(new PropertyContainer(buyingWorkResistTmp), new PropertyContainer(learningResistTmp), timeTmp)
    {
        resists.Add(new PropertyContainer(writeOffResist));
    }

}
public interface UpdateSubscriber
{
    public void ChangeResist(ObjectiveImpl objective);
    public void SetController(Controller controller);
}

public abstract class IncreasingResist : UpdateSubscriber
{
    protected Controller controller;
    public abstract void ChangeResist(ObjectiveImpl objective);
    public void SetController(Controller controller)
    {
        this.controller = controller;
    }
}
public class Teacher : IncreasingResist
{
    public Teacher(string tmpFirstName, string tmpSecondName, Controller controller)
    {
        firstName = tmpFirstName;
        secondName = tmpSecondName;
        loyalty = 0;
        isAssistant = false;
        decreaseModifier = 1;
        this.controller = controller;
    }
    protected int decreaseModifier = 1;
    public void SetDecreaseModifier(int decreaseModifier)
    {
        this.decreaseModifier = decreaseModifier;
    }
    public Teacher ShallowCopy()
    {
        return (Teacher)MemberwiseClone();
    }

    public Teacher DeepCopy()
    {
        Teacher lecturer = (Teacher)MemberwiseClone();
        lecturer.firstName = String.Copy(firstName);
        lecturer.secondName = String.Copy(secondName);

        lecturer.loyalty = loyalty;
        lecturer.isAssistant = isAssistant;
        lecturer.decreaseModifier = decreaseModifier;

        return lecturer;
    }

    public bool isAssistant { get; set; }

    string firstName;
    string secondName;

    public int loyalty { get; set; }

    public void SetMemento(TeacherMemento State)
    {
        loyalty = State.loyalty;
        firstName = State.firstName;
        secondName = State.secondName;
        isAssistant = State.isAssistant;
    }

    public TeacherMemento CreateMemento()
    {
        return new TeacherMemento(firstName, secondName, loyalty, isAssistant);
    }

    Random rnd = new Random();

    void Increase(PropertyContainer resist)
    {
        if (resist.resistNumber < 10)
        {
            resist.resistNumber += rnd.Next(1, 3) / decreaseModifier;
        }
        if (resist.resistNumber > 10)
        {
            resist.resistNumber = 10;
        }
    }

    public override void ChangeResist(ObjectiveImpl objective)
    {
        List<PropertyContainer> props = objective.getResists();
        int resistToIncrease = rnd.Next(1, objective.getResists().Count + 1);
        Increase(props[resistToIncrease - 1]);
    }
}

public class TeacherMemento
{
    public TeacherMemento(string state1, string state2, int state3, bool state4)
    {
        firstName = state1;
        secondName = state2;
        loyalty = state3;
        isAssistant = state4;
    }

    public string firstName { get; set; }
    public string secondName { get; set; }
    public int loyalty { get; set; }
    public bool isAssistant { get; set; }
}

public class TeacherFactory
{
    private static Dictionary<string, Teacher> teachers = new Dictionary<string, Teacher>();

    public Teacher GetTeacher(string firstname, string lastname, Controller controller)
    {
        Teacher teacher = teachers[firstname + lastname];
        if (teacher == null)
        {
            teacher = new Teacher(firstname, lastname, controller);
            teachers.Add(firstname + lastname, teacher);
        }
        return teacher;
    }
}
public interface IncreasingResistMediator
{
    void setDecreaseModifier(int decreaseModifier);
    void increaseRandomResist(ObjectiveImpl objective);
}
public class TeacherMediator : IncreasingResistMediator
{
    private Teacher Teacher;
    public void setDecreaseModifier(int decreaseModifier)
    {
        Teacher.SetDecreaseModifier(decreaseModifier);
    }
    public void increaseRandomResist(ObjectiveImpl objective)
    {
        Teacher.ChangeResist(objective);
    }
    public TeacherMediator(Teacher teacher)
    {
        this.Teacher = teacher;
    }
}
public class IncreasingResistContext : IncreasingResist
{
    private IncreasingResistMediator mediator;
    private IncreasingResistState teacherState;

    public IncreasingResistContext(IncreasingResistMediator mediator, IncreasingResistState state)
    {
        this.mediator = mediator;
        state.setContext(this);

    }
    public IncreasingResistMediator getMediator()
    {
        return mediator;
    }
    public void changeState(IncreasingResistState state)
    {
        teacherState = state;
    }
    public override void ChangeResist(ObjectiveImpl objective)
    {
        teacherState.changeResist(objective);
    }
}
public interface IncreasingResistState
{
    void setContext(IncreasingResistContext context);
    void changeResist(ObjectiveImpl objective);
}
public class LecturerState : IncreasingResistState
{
    private IncreasingResistContext context;
    public void setContext(IncreasingResistContext context)
    {
        this.context = context;
    }
    public void changeResist(ObjectiveImpl objective)
    {
        context.getMediator().setDecreaseModifier(1);
        context.getMediator().increaseRandomResist(objective);
    }
}
public class AssistantState : IncreasingResistState
{
    private IncreasingResistContext context;
    public void setContext(IncreasingResistContext context)
    {
        this.context = context;
    }
    public void changeResist(ObjectiveImpl objective)
    {
        context.getMediator().setDecreaseModifier(2);
        context.getMediator().increaseRandomResist(objective);
    }
}
public class Player
{
    private static Player instance;
    Random rnd;
    public ObjectiveImpl currentObjective { get; set; }
    public int currentSuccessChance { get; set; }
    private Player()
    {
        rnd = new Random();

        currentSuccessChance = 0;
    }

    public static Player Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new Player();
            }
            return instance;
        }
    }

    public PlayerMemento CreateMemento()
    {
        return new PlayerMemento(currentObjective.CreateMemento(), currentSuccessChance);
    }

    public void SetMemento(PlayerMemento State)
    {
        currentObjective.SetMemento(State.currentObjective);
        currentSuccessChance = State.currentSuccessChance;
    }

    public void PerformAction(int actionIndex, ObjectiveImpl chosenObjective)
    {
        int successChance = rnd.Next(101 - chosenObjective.getResists()[actionIndex].resistNumber);
        ChangeSuccessChance(successChance);
    }

    void ChangeSuccessChance(int successChance)
    {
        if (successChance > 70)
        {
            currentSuccessChance += 10;
        }
        else if (successChance > 20)
        {
            currentSuccessChance += 5;
        }
        else
        {
            currentSuccessChance -= 5;
        }
    }
}

public class PlayerMemento
{
    public PlayerMemento(ObjectiveImplMemento state1, int state2)
    {
        currentObjective = state1;
        currentSuccessChance = state2;
    }
    public ObjectiveImplMemento currentObjective { get; set; }
    public int currentSuccessChance { get; set; }
}

public class Dopusk
{
    private Random rnd;

    public Dopusk()
    {
        isCompleted = false;
        rnd = new Random();
    }

    public void Try()
    {
        if (rnd.Next(1, 5) < 3)
        {
            isCompleted = true;
        }
    }

    public bool isCompleted { get; set; }

    public void SetMemento(DopuskMemento State)
    {
        isCompleted = State.isCompleted;
    }

    public DopuskMemento CreateMemento()
    {
        return new DopuskMemento(isCompleted);
    }
}

public class DopuskMemento
{
    public DopuskMemento(bool state1)
    {
        isCompleted = state1;
    }

    public bool isCompleted { get; set; }
}

public abstract class AbstractClassmates
{
    public abstract void Operation1();
    public abstract void Operation2();
    public abstract void Operation3();

    public void TemplateMethod()
    {
        Operation1();
        Operation2();
        Operation3();
    }
}
public class Classmates : AbstractClassmates
{
    public Classmates(IStrategy chosenStrategy)
    {
        rnd = new Random();
        groupSize = 15;
        successModifier = 0;
        int loyalty = 0;
        Strategy = chosenStrategy;
    }

    int successModifier { get; set; }

    int groupSize;
    int loyalty;

    Random rnd;

    public int getGroupSize()
    {
        return groupSize;
    }

    IStrategy Strategy { get; set; }

    public void PerformAction()
    {
        Strategy.Action(ref groupSize);
    }

    public void ChangeLoyalty()
    {
        if (rnd.Next(1, 2) < 2)
        {
            loyalty--;
        }
        else
        {
            loyalty++;
        }
    }

    public void CalculateSuccessModifier()
    {
        successModifier = groupSize + loyalty;
    }

    public override void Operation1()
    {
        PerformAction();
    }

    public override void Operation2()
    {
        ChangeLoyalty();
    }

    public override void Operation3()
    {
        CalculateSuccessModifier();
    }

    public void SetMemento(ClassmatesMemento State)
    {
        successModifier = State.successModifier;

        groupSize = State.groupSize;
        loyalty = State.loyalty;
    }

    public ClassmatesMemento CreateMemento()
    {
        return new ClassmatesMemento(successModifier, groupSize, loyalty);
    }
}

public class ClassmatesMemento
{
    public ClassmatesMemento(int state1, int state2, int state3)
    {
        successModifier = state1;
        groupSize = state2;
        loyalty = state3;
    }
    public int successModifier { get; set; }
    public int groupSize { get; set; }
    public int loyalty { get; set; }
}


public interface IStrategy
{
    public void Action(ref int group);
}

public class ConcreteStrategy1 : IStrategy
{
    public void Action(ref int group)
    {
        group--;
    }
}

public class ConcreteStrategy2 : IStrategy
{
    public void Action(ref int group)
    {
        group++;
    }
}

public interface TeammateChain : UpdateSubscriber
{
    void SetNextTeammate(Teammate teammate);
    void ChangeResist(ObjectiveImpl objective);
}
public class Teammate : TeammateChain
{
    public void SetController(Controller controller)
    {
        this.controller = controller;
    }
    private Controller controller;
    private Random rnd;
    private Teammate nextTeammate;
    public void SetNextTeammate(Teammate teammate)
    {
        this.nextTeammate = teammate;
    }
    public Teammate GetNextTeammate()
    {
        return nextTeammate;
    }

    public Teammate(Controller controller)
    {
        rnd = new Random();
        this.controller = controller;
    }

    public void ChangeResist(ObjectiveImpl objectiveToIncrease)
    {
        List<PropertyContainer> props = objectiveToIncrease.getResists();
        int resistToIncrease = rnd.Next(1, objectiveToIncrease.getResists().Count + 1);
        Decrease(props[resistToIncrease - 1]);
        nextTeammate.ChangeResist(objectiveToIncrease);
    }

    void Decrease(PropertyContainer resist)
    {
        if (rnd.Next(0, 2) != 0)
        {
            if (resist.resistNumber < 10 || resist.resistNumber > 0)
            {
                resist.resistNumber -= 1;
            }
        }
    }
}

public abstract class Subject
{
    public abstract string getActions();
    public Teacher lecturer { get; set; }
    protected Player mainPlayer;

    public SubjectMemento CreateMemento()
    {
        return new SubjectMemento(lecturer.CreateMemento(), mainPlayer.CreateMemento());
    }

    public void SetMemento(SubjectMemento State)
    {
        lecturer.SetMemento(State.lecturer);
    }
}

public class SubjectMemento
{
    public SubjectMemento(TeacherMemento state1, PlayerMemento state2)
    {
        lecturer = state1;
        mainPlayer = state2;
    }

    public TeacherMemento lecturer { get; set; }
    public PlayerMemento mainPlayer { get; set; }
}

public abstract class SubjectWithLabs : Subject
{
    protected Classmates classmates;
    public override string getActions()
    {
        return "0 - Learn\n1 - Buy\n2 - Work in group";
    }

    public SubjectWithLabsMemento CreateMemento()
    {
        return new SubjectWithLabsMemento(lecturer.CreateMemento(), mainPlayer.CreateMemento(), classmates.CreateMemento());
    }

    public void SetMemento(SubjectWithLabsMemento State) 
    {
        base.SetMemento(State);
        classmates.SetMemento(State.classmates);
    }
}

public class SubjectWithLabsMemento : SubjectMemento
{
    public SubjectWithLabsMemento(TeacherMemento state1, PlayerMemento state2, ClassmatesMemento state3) : base(state1, state2)
    {
        classmates = state3;
    }

    public ClassmatesMemento classmates;
}

public class SubjectsWithLabsWithAssistant : SubjectWithLabs
{
    public SubjectsWithLabsWithAssistant(Teacher lecturerTmp, Player mainPlayerTmp, Classmates classmatesTmp,
        List<Lab> labsTmp, Teacher labAssistantTmp)
    {
        lecturer = lecturerTmp;
        mainPlayer = mainPlayerTmp;
        classmates = classmatesTmp;
        labs = labsTmp;
        labAssistant = labAssistantTmp;
        labAssistant.isAssistant = true;

        foreach (Lab obj in labs)
        {
            obj.getResists()[2].resistNumber -= classmates.getGroupSize() / 10;
        }
    }
    public List<Lab> labs { get; set; }
    public Teacher labAssistant { get; set; }

    public SubjectWithLabsWithAssistantMemento CreateMemento()
    {
        List<ObjectiveImplMemento> tmpList = new List<ObjectiveImplMemento>();
        for(int i = 0; i < labs.Count; i++)
        {
            tmpList.Add(labs[i].CreateMemento());
        }
        return new SubjectWithLabsWithAssistantMemento(lecturer.CreateMemento(), mainPlayer.CreateMemento(), classmates.CreateMemento(), tmpList, labAssistant.CreateMemento());
    }

    public void SetMemento(SubjectWithLabsWithAssistantMemento State)
    {
        base.SetMemento(State);
        for (int i = 0; i < State.labs.Count; i++)
        {
            labs[i].SetMemento(State.labs[i]);
        }
        labAssistant.SetMemento(State.labAssistant);
    }
}

public class SubjectWithLabsWithAssistantMemento : SubjectWithLabsMemento
{
    public SubjectWithLabsWithAssistantMemento(TeacherMemento state1, PlayerMemento state2, ClassmatesMemento state3, 
        List<ObjectiveImplMemento> state4, TeacherMemento state5) : base(state1, state2, state3)
    {
        labs = state4;
        labAssistant = state5;
    }

    public List<ObjectiveImplMemento> labs { get; set; }
    public TeacherMemento labAssistant { get; set; }
}

public class SubjectsWithLabsWithoutAssistant : SubjectWithLabs
{
    public SubjectsWithLabsWithoutAssistant(Teacher lecturerTmp, Player mainPlayerTmp, Classmates classmatesTmp, Dictionary<Lab, Dopusk> labsTmp)
    {
        lecturer = lecturerTmp;
        mainPlayer = mainPlayerTmp;
        classmates = classmatesTmp;
        labs = labsTmp;

        foreach (Lab obj in labs.Keys)
        {
            obj.getResists()[2].resistNumber -= classmates.getGroupSize() / 10;
        }
    }

    public Dictionary<Lab, Dopusk> labs { get; set; }

    public SubjectsWithLabsWithoutAssistantMemento CreateMemento()
    {
        Dictionary<ObjectiveImplMemento, DopuskMemento> tmpDictionary = new Dictionary<ObjectiveImplMemento, DopuskMemento>();
        for (int i = 0; i < labs.Count; i++)
        {
            tmpDictionary.Add(labs.Keys.ElementAt(i).CreateMemento(), labs[labs.Keys.ElementAt(i)].CreateMemento());
        }
        return new SubjectsWithLabsWithoutAssistantMemento(lecturer.CreateMemento(), mainPlayer.CreateMemento(), classmates.CreateMemento(), 
            tmpDictionary);
    }

    public void SetMemento(SubjectsWithLabsWithoutAssistantMemento State)
    {
        base.SetMemento(State);
        for (int i = 0; i < State.labs.Count; i++)
        {
            labs.Keys.ElementAt(i).SetMemento(State.labs.Keys.ElementAt(i));
            labs[labs.Keys.ElementAt(i)].SetMemento(State.labs[State.labs.Keys.ElementAt(i)]);
        }
    }
}

public class SubjectsWithLabsWithoutAssistantMemento : SubjectWithLabsMemento
{
    public SubjectsWithLabsWithoutAssistantMemento(TeacherMemento state1, PlayerMemento state2, ClassmatesMemento state3,
        Dictionary<ObjectiveImplMemento, DopuskMemento> state4) : base(state1, state2, state3)
    {
        labs = state4;
    }

    public Dictionary<ObjectiveImplMemento, DopuskMemento> labs { get; set; }
}

public abstract class SubjectWithCoursework : Subject
{
    public Coursework coursework { get; set; }
    public override string getActions()
    {
        return "0 - Learn\n1 - Buy\n2 - Write off";
    }
    public SubjectWithCourseworkMemento CreateMemento()
    {
        return new SubjectWithCourseworkMemento(lecturer.CreateMemento(), mainPlayer.CreateMemento(), coursework.CreateMemento());
    }

    public void SetMemento(SubjectWithCourseworkMemento State)
    {
        base.SetMemento(State);
        coursework.SetMemento(State.coursework);
    }
}

public class SubjectWithCourseworkMemento : SubjectMemento
{
    public SubjectWithCourseworkMemento(TeacherMemento state1, PlayerMemento state2, ObjectiveImplMemento state3) : base(state1, state2)
    {
        coursework = state3;
    }

    public ObjectiveImplMemento coursework { get; set; }
}

public class SubjectWithCourseworkSolo : SubjectWithCoursework
{
    public SubjectWithCourseworkSolo(Teacher lecturerTmp, Player mainPlayerTmp, Coursework courseworkTmp)
    {
        lecturer = lecturerTmp;
        mainPlayer = mainPlayerTmp;
        coursework = courseworkTmp;
    }
}

public static class PostponeDeadline
{
    public static void askForPostponement(this Player mainPlayer, SubjectWithCourseworkSolo subjectToPostpone)
    {
        Random rnd = new Random();
        int timeToIncrease = rnd.Next(1, 5);
        subjectToPostpone.coursework.time -= timeToIncrease;
    }
}

public class SubjectWithCourseworkTeamwork : SubjectWithCoursework
{
    public SubjectWithCourseworkTeamwork(Teacher lecturerTmp, Player mainPlayerTmp, Coursework courseworkTmp, TeammateChain teamTmp)
    {
        lecturer = lecturerTmp;
        mainPlayer = mainPlayerTmp;
        coursework = courseworkTmp;
        team = teamTmp;
    }

    public TeammateChain team { get; set; }
}

public enum SubjectType
{
    SUBJECT_WITH_LABS_AND_ASSITANT, SUBJECT_WITH_LABS_AND_LECTURER, SUBJECT_WITH_COURSEWORK_SOLO, SUBJECT_WITH_COURSEWORK_GROUP
}
public class SubjectFacade
{
    public Subject createSubject(SubjectType subjectType, Teacher teacher, Player player, Controller controller)
    {
        IAbstractFactory factory;
        switch (subjectType)
        {
            case SubjectType.SUBJECT_WITH_LABS_AND_ASSITANT:
                factory = new ConcreteFactory2();
                return factory.CreateSubjectWithLabs(teacher, player, controller);
            case SubjectType.SUBJECT_WITH_LABS_AND_LECTURER:
                factory = new ConcreteFactory1();
                return factory.CreateSubjectWithLabs(teacher, player, controller);
            case SubjectType.SUBJECT_WITH_COURSEWORK_SOLO:
                factory = new ConcreteFactory1();
                return factory.CreateSubjectWithCoursework(teacher, player, controller);
            case SubjectType.SUBJECT_WITH_COURSEWORK_GROUP:
                factory = new ConcreteFactory2();
                return factory.CreateSubjectWithCoursework(teacher, player, controller);
        }

        return null;
    }
}
public interface IAbstractFactory
{
    public SubjectWithLabs CreateSubjectWithLabs(Teacher lecturerTmp, Player mainPlayerTmp, Controller controller);
    public SubjectWithCoursework CreateSubjectWithCoursework(Teacher lecturerTmp, Player mainPlayerTmp, Controller controller);

}

public class ConcreteFactory1 : IAbstractFactory
{
    public SubjectWithCoursework CreateSubjectWithCoursework(Teacher lecturerTmp, Player mainPlayerTmp, Controller controller)
    {
        return new SubjectWithCourseworkSolo(lecturerTmp, mainPlayerTmp, new Coursework(5, 5, 5, 100));
    }
    //_lecturer, Player _mainPlayer, Coursework _coursework
    public SubjectWithLabs CreateSubjectWithLabs(Teacher lecturerTmp, Player mainPlayerTmp, Controller controller)
    {
        return new SubjectsWithLabsWithoutAssistant(lecturerTmp, mainPlayerTmp, new Classmates(new ConcreteStrategy1()), new Dictionary<Lab, Dopusk>());
    }
}


public class ConcreteFactory2 : IAbstractFactory
{
    public SubjectWithCoursework CreateSubjectWithCoursework(Teacher lecturerTmp, Player mainPlayerTmp, Controller controller)
    {
        return new SubjectWithCourseworkTeamwork(lecturerTmp, mainPlayerTmp, new Coursework(5, 5, 5, 100), new Teammate(controller));
    }

    public SubjectWithLabs CreateSubjectWithLabs(Teacher lecturerTmp, Player mainPlayerTmp, Controller controller)
    {
        return new SubjectsWithLabsWithAssistant(lecturerTmp, mainPlayerTmp, new Classmates(new ConcreteStrategy2()), new List<Lab>(), new Teacher("Empty", "Empty", controller));
    }
}

public class ControllerMemento
{
    public List<TeacherMemento> Department;
    public PlayerMemento mainPlayer;
    public List<SubjectMemento> allSubjects;
    public SubjectMemento currentLevel { get; set; }
    public int triesCount;
    public bool isWorkingOnLab;
    public ObjectiveImplMemento currentLab;
    public DopuskMemento currentLabDopusk;
    public int isAskingForPostponement;
    public int levelType;

    public ControllerMemento(List<TeacherMemento> state1, PlayerMemento state2, List<SubjectMemento> state3, SubjectMemento state4,
        int state5, bool state6, ObjectiveImplMemento state7, DopuskMemento state8, int state9, int state10)
    {
        Department = state1;
        mainPlayer = state2;
        allSubjects = state3;
        currentLevel = state4;
        triesCount = state5;
        isWorkingOnLab = state6;
        currentLab = state7;
        currentLabDopusk = state8;
        isAskingForPostponement = state9;
        levelType = state10;
}

}

public class Controller
{
    Random rnd;

    List<Teacher> Department;

    ConcreteFactory1 firstLevel;
    ConcreteFactory2 secondLevel;

    Player mainPlayer;

    List<Subject> allSubjects;

    public Subject currentLevel { get; set; }

    int triesCount;
    int levelIndex;
    bool isWorkingOnLab;
    bool isLevelCompleted;
    Lab currentLab;
    Dopusk currentLabDopusk;
    int isAskingForPostponement;
    int levelType;

    public ControllerMemento CreateMemento()
    {
        List<TeacherMemento> tmpList1 = new List<TeacherMemento>();
        List<SubjectMemento> tmpList2 = new List<SubjectMemento>();

        for(int i = 0; i < Department.Count; i++)
        {
            tmpList1.Add(Department[i].CreateMemento());
        }

        for(int i = 0; i < allSubjects.Count; i++)
        {
            tmpList2.Add(allSubjects[i].CreateMemento());
        }

        return new ControllerMemento(tmpList1, mainPlayer.CreateMemento(), tmpList2, currentLevel.CreateMemento(), triesCount, isWorkingOnLab, 
            currentLab.CreateMemento(), currentLabDopusk.CreateMemento(), isAskingForPostponement, levelType);
    }

    public void SetMemento(ControllerMemento State)
    {
        for(int i = 0; i < State.Department.Count; i++)
        {
            Department[i].SetMemento(State.Department[i]);
        }
        
        for(int i = 0; i < State.allSubjects.Count; i++)
        {
            allSubjects[i].SetMemento(State.allSubjects[i]);
        }

        mainPlayer.SetMemento(State.mainPlayer);    
        currentLevel.SetMemento(State.currentLevel);
        triesCount = State.triesCount;
        isWorkingOnLab = State.isWorkingOnLab;
        currentLab.SetMemento(State.currentLab);
        currentLabDopusk.SetMemento(State.currentLabDopusk);
        isAskingForPostponement = State.isAskingForPostponement;
        levelType = State.levelType;
    }

    private List<UpdateSubscriber> updateSubscribers = new List<UpdateSubscriber>();
    public void addSubscriber(UpdateSubscriber updateSubscriber)
    {
        updateSubscribers.Add(updateSubscriber);
    }
    public void removeSubcriber(UpdateSubscriber updateSubscriber)
    {
        updateSubscribers.Remove(updateSubscriber);
    }
    public Controller()
    {
        rnd = new Random();
        Department = new List<Teacher>();

        triesCount = rnd.Next(1, 10);

        Dictionary<Lab, Dopusk> labDictionaryTmp = new Dictionary<Lab, Dopusk>();
        for (int i = 0; i < rnd.Next(0, 10); i++)
        {
            labDictionaryTmp.Add(new Lab(5, 5, 5, 10), new Dopusk());
        }

        List<Lab> labListTmp = new List<Lab>();
        for (int i = 0; i < rnd.Next(0, 10); i++)
        {
            labListTmp.Add(new Lab(5, 5, 5, 10));
        }
        Teammate firstTeammate = new Teammate(this);
        Teammate currentTeammate = firstTeammate;
        for (int i = 0; i < rnd.Next(0, 4); i++)
        {
            currentTeammate.SetNextTeammate(new Teammate(this));
            currentTeammate = currentTeammate.GetNextTeammate();
        }

        Department.Add(new Teacher("Mykola", "Ivanenko", this));
        Department.Add(Department.First().ShallowCopy());
        Department.Add(Department.First().DeepCopy());
        Department.Add(new Teacher("Serhii", "Dubovyi", this));
        Department.Add(new Teacher("Olexandr", "Ptashka", this));
        Department.Add(new Teacher("Markiyan", "Stepovyi", this));
        Department.Add(new Teacher("Oleh", "Yakiv", this));

        mainPlayer = Player.Instance;
        allSubjects = new List<Subject>();
        firstLevel = new ConcreteFactory1();
        secondLevel = new ConcreteFactory2();

        allSubjects.Add(firstLevel.CreateSubjectWithCoursework(Department[rnd.Next(0, Department.Count)], mainPlayer, this));

        allSubjects.Add(firstLevel.CreateSubjectWithLabs(Department[rnd.Next(0, Department.Count)], mainPlayer, this));
        ((SubjectsWithLabsWithoutAssistant)allSubjects[1]).labs = labDictionaryTmp;

        allSubjects.Add(secondLevel.CreateSubjectWithLabs(Department[rnd.Next(0, Department.Count)], mainPlayer, this));
        ((SubjectWithCourseworkTeamwork)allSubjects[2]).team = firstTeammate;

        allSubjects.Add(secondLevel.CreateSubjectWithCoursework(Department[rnd.Next(0, Department.Count)], mainPlayer, this));
        allSubjects.Add(firstLevel.CreateSubjectWithLabs(Department[rnd.Next(0, Department.Count)], mainPlayer, this));
        ((SubjectsWithLabsWithAssistant)allSubjects[3]).labs = labListTmp;

        levelIndex = rnd.Next(0, allSubjects.Count - 1);
        currentLevel = allSubjects[rnd.Next(0, allSubjects.Count - 1)];

        isWorkingOnLab = false;
        isLevelCompleted = false;
    }

    public void Update()
    {
        while (true)
        {
            Console.WriteLine("Choose an action to do:");
            Console.WriteLine(currentLevel.getActions());
            int chosenAction = Int32.Parse(Console.ReadLine());
            Thread.Sleep(1000);
            ObjectiveImpl currentObjective = null;
            if (currentLevel is SubjectsWithLabsWithoutAssistant)
            {
                levelType = 1;
                if (!isWorkingOnLab)
                {
                    currentLab = ((SubjectsWithLabsWithoutAssistant)currentLevel).labs.Keys.First();
                    currentLabDopusk = ((SubjectsWithLabsWithoutAssistant)currentLevel).labs[currentLab];
                    ((SubjectsWithLabsWithoutAssistant)currentLevel).labs.Remove(currentLab);

                    isWorkingOnLab = true;
                }

                if (currentLabDopusk.isCompleted)
                {
                    mainPlayer.PerformAction(chosenAction, currentLab);
                    currentObjective = currentLab;
                }
                else
                {
                    currentLabDopusk.Try();
                }
            }
            else if (currentLevel is SubjectWithCourseworkSolo)
            {
                levelType = 2;
                isAskingForPostponement = rnd.Next(0, 1);
                if (isAskingForPostponement == 0)
                {
                    if (triesCount > 0)
                    {
                        triesCount--;
                        mainPlayer.askForPostponement((SubjectWithCourseworkSolo)currentLevel);
                    }
                }

                mainPlayer.PerformAction(chosenAction, ((SubjectWithCoursework)currentLevel).coursework);

                currentObjective = ((SubjectWithCoursework)currentLevel).coursework;
            }
            else if (currentLevel is SubjectWithCourseworkTeamwork)
            {
                levelType = 2;
                mainPlayer.PerformAction(chosenAction, ((SubjectWithCoursework)currentLevel).coursework);
                ((SubjectWithCourseworkTeamwork)currentLevel).team.ChangeResist(((SubjectWithCoursework)currentLevel).coursework);


                currentObjective = ((SubjectWithCoursework)currentLevel).coursework;
            }
            else
            {
                levelType = 1;
                if (!isWorkingOnLab)
                {
                    if (((SubjectsWithLabsWithAssistant)currentLevel).labs.Count != 0)
                    {
                        currentLab = ((SubjectsWithLabsWithAssistant)currentLevel).labs.First();
                        ((SubjectsWithLabsWithAssistant)currentLevel).labs.Remove(currentLab);

                        isWorkingOnLab = true;
                    }
                }

                mainPlayer.PerformAction(chosenAction, currentLab);
                currentObjective = currentLab;
            }
            foreach (UpdateSubscriber updateSubcriber in updateSubscribers)
            {
                updateSubcriber.ChangeResist(currentObjective);
            }
            if (levelType == 1)
            {
                if (currentLab.time > 0)
                {
                    currentLab.time--;
                }
                else
                {
                    if (isWorkingOnLab)
                    {
                        if (mainPlayer.currentSuccessChance > 50)
                        {
                            if (mainPlayer.currentSuccessChance > 85)
                            {
                                currentLevel.lecturer.loyalty++;
                            }

                            isWorkingOnLab = false;
                        }
                        else
                        {
                            Console.WriteLine("Game over, you lose.");
                            break;
                        }
                    }
                    else
                    {
                        removeSubcriber(currentLevel.lecturer);
                        if (currentLevel is SubjectsWithLabsWithAssistant)
                        {
                            removeSubcriber(((SubjectsWithLabsWithAssistant)currentLevel).labAssistant);
                        }
                        else if (currentLevel is SubjectWithCourseworkTeamwork)
                        {
                            removeSubcriber(((SubjectWithCourseworkTeamwork)currentLevel).team);
                        }
                        allSubjects.Remove(currentLevel);
                        if (allSubjects.Count != 0)
                        {
                            currentLevel = allSubjects[rnd.Next(0, allSubjects.Count - 1)];
                            addSubscriber(currentLevel.lecturer);
                            if (currentLevel is SubjectsWithLabsWithAssistant)
                            {
                                addSubscriber(((SubjectsWithLabsWithAssistant)currentLevel).labAssistant);
                            }
                            else if (currentLevel is SubjectWithCourseworkTeamwork)
                            {
                                addSubscriber(((SubjectWithCourseworkTeamwork)currentLevel).team);
                            }
                        }
                        else
                        {
                            Console.WriteLine("Congratulations, you won!");
                            break;
                        }
                    }
                }
            }
            else
            {
                if (((SubjectWithCoursework)currentLevel).coursework.time > 0)
                {
                    ((SubjectWithCoursework)currentLevel).coursework.time--;
                }
                else
                {
                    if (mainPlayer.currentSuccessChance > 50)
                    {
                        if (mainPlayer.currentSuccessChance > 85)
                        {
                            currentLevel.lecturer.loyalty++;
                        }
                        removeSubcriber(currentLevel.lecturer);
                        if (currentLevel is SubjectsWithLabsWithAssistant)
                        {
                            removeSubcriber(((SubjectsWithLabsWithAssistant)currentLevel).labAssistant);
                        }
                        else if (currentLevel is SubjectWithCourseworkTeamwork)
                        {
                            removeSubcriber(((SubjectWithCourseworkTeamwork)currentLevel).team);
                        }
                        allSubjects.Remove(currentLevel);
                        if (allSubjects.Count != 0)
                        {
                            currentLevel = allSubjects[rnd.Next(0, allSubjects.Count - 1)];
                            addSubscriber(currentLevel.lecturer);
                            if (currentLevel is SubjectsWithLabsWithAssistant)
                            {
                                addSubscriber(((SubjectsWithLabsWithAssistant)currentLevel).labAssistant);
                            }
                            else if (currentLevel is SubjectWithCourseworkTeamwork)
                            {
                                addSubscriber(((SubjectWithCourseworkTeamwork)currentLevel).team);
                            }
                        }
                        else
                        {
                            Console.WriteLine("Congratulations, you won!");
                            break;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Game over, you lose.");
                        break;
                    }
                }
            }
        }
    }
}
